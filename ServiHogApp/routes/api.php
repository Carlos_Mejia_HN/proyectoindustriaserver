<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('registro','AuthController@registrar_usuario');
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::post('user/register', 'APIRegisterController@register');
// // Route::post('user/login', 'APILoginController@login');
// Route::group([
//         'middleware' => ['api', 'cors'],
//         //'namespace' => $this->namespace,
//         'prefix' => 'api/auth',
//     ], function ($router) {
//          //Add you routes here, for example:
//          Route::apiResource('/posts','PostController');
//          Route::post('login', 'AuthController@login');
//          Route::post('logout', 'AuthController@logout');
//          Route::post('refresh', 'AuthController@refresh');
//          Route::post('obtener_usuarios', 'UserController@obtener_usuarios');
//     });
