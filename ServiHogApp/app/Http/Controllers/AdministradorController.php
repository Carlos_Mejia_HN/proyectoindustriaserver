<?php

namespace App\Http\Controllers;
use JWTFactory;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use App\User;
use App\Rol;
use App\Servicio;
use App\CategoriaServicio;
use Illuminate\Http\Request;

class AdministradorController extends Controller
{
    public function get_usuarios(){
    	$usuarios = User::all();
	    if (count($usuarios) >= 1) {
	    	for ($i=0; $i < count($usuarios); $i++) { 
	    		$usuarios[$i]['id_rol'] = Rol::find($usuarios[$i]['id_rol']);
	    		$destino = "imgs/users";
             	$usuarios[$i]['foto_perfil'] = url($destino, $usuarios[$i]['foto_perfil']);
	    	}
	    	return response()->json([
	        	'validate' => true,
	        	'data' => $usuarios,
	    	]);
	    }
	    else{
	    	return response()->json([
	        	'validate' => false,
	        	'message' => 'No se encontraron usuarios',
	    	]);
	    }
    }

    public function get_servicios(){
    	$servicios = Servicio::all();
    	if (count($servicios) >= 1) {
	    	for ($i=0; $i < count($servicios); $i++) { 
	    		$servicios[$i]['id_categoria'] = CategoriaServicio::find($servicios[$i]['id_categoria']);
	    		$destino = "imgs/servicios";
             	$servicios[$i]['imagen'] = url($destino, $servicios[$i]['imagen']);
	    	}
	    	return response()->json([
	        	'validate' => true,
	        	'data' => $servicios,
	    	]);
	    }
	    else{
	    	return response()->json([
	        	'validate' => false,
	        	'message' => 'No se encontraron servicios',
	    	]);
	    }	
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function actualizar_usuario(Request $request){
    	$solicitud = $request->only(
            'nombre',
            'apellido',
            'sexo',
            'correo',
            'telefono',
            'id_rol',
            'password',
            'foto_perfil',
            'id'
        );        	
        try{
        	$user = User::find($solicitud['id']);
    		$nombreImg = '';
    		if ($request->hasFile('foto_perfil')) {
    			$destino = "imgs/users";
        		$nombreImg = "user".'_'.$user->id.'_'
        		.$request->file('foto_perfil')->getClientOriginalName();
        		$request->file('foto_perfil')
        		->move($destino, $nombreImg);
    		}
        	$user->foto_perfil = $nombreImg;
            $user->nombre = $solicitud['nombre'];
            $user->apellido = $solicitud['apellido'];
            $user->sexo = $solicitud['sexo'];
            $user->email = $solicitud['correo'];
            $user->telefono = $solicitud['telefono'];
            $user->id_rol = $solicitud['id_rol'];
            $user->password = $user->password;
            if ($solicitud['password'] != '') {
            	$user->password = bcrypt($solicitud['password']);
            }
            $user->update();
            return response()->json([
                'validate' => true,
                'message' => 'Usuario actualizado exitosamente.'
            ]);
        }
        catch(Exception $e){
        	return response()->json([
                'validate' => false,
                'message' => 'Problemas con el servidor.'
            ]);
        }
    }
}
