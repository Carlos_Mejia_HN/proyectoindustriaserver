<?php

namespace App\Http\Controllers;
use App\Rol;
use App\CategoriaServicio;
use App\Servicio;
use App\Colaborador;
use App\User;
use Illuminate\Http\Request;
use JWTFactory;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Solicitude;
use Carbon\Carbon;

class ClienteController extends Controller
{
    public function getCategorias(){
      $categorias = CategoriaServicio::all();
      for ($i=0; $i < count($categorias); $i++) { 
         $destino = "imgs/categorias";
         $categorias[$i]['imagen'] = url($destino, $categorias[$i]['imagen']);
      }
      return response()->json([
          'validate' => true,
          'data' => $categorias,
      ]);
    }

    public function getServicios(Request $request){
      $validator = Validator::make($request->all(), [
          'id' => 'required',
          'descripcion'=> 'required'
      ]);
      if ($validator->fails()) {
        $fail = "";
        foreach ($validator->errors()->all() as $key => $error) {
            $key++;
            $fail .= "".$error."\n";
        }
        return response()->json([
            'validate' => false,
            'message' => $fail
        ]);
      }
      else {
        $servicios = Servicio::orderBy('id', 'ASC')->where('id_categoria',$request->get('id'))->get();
        for ($i=0; $i < count($servicios); $i++) { 
          $destino = "imgs/servicios";
          $servicios[$i]['imagen'] = url($destino, $servicios[$i]['imagen']);
        }
        return response()->json([
            'validate' => true,
            'data' => $servicios
        ]);
      }
    }

    public function obtener_colaboradores(Request $request)
    {
      $validator = Validator::make($request->all(), [
          'id' => 'required',
          'descripcion'=> 'required'
      ]);
      if ($validator->fails()) {
        $fail = "";
        foreach ($validator->errors()->all() as $key => $error) {
            $key++;
            $fail .= "".$error."\n";
        }
        return response()->json([
            'validate' => false,
            'message' => $fail
        ]);
      }
    else {
        $Colaboradores = Colaborador::orderBy('id', 'ASC')->where('id_servicio',$request->get('id'))->get();
        if (count($Colaboradores) <= 0) {
          return response()->json([
            'validate' => false,
            'message' => 'No hay colaboradores disponibles para este servicio en tu area', 
          ]);
        }
        else{
          for ($i=0; $i < count($Colaboradores); $i++) { 
             $user = User::find($Colaboradores[$i]['id_usuario']);
             $destino = "imgs/users";
             $user->foto_perfil = url($destino, $user->foto_perfil);
             $Colaboradores[$i]['id_usuario'] = $user;
          }
          return response()->json([
            'validate' => true,
            'data' => $Colaboradores
          ]);
        }
      }
    }

    public function contratar_colaborador(Request $request){
      $validator = Validator::make($request->all(), [
          'descripcion_solicitud' => 'required',
          'fecha'=> 'required',
          'colaborador' => 'required'
      ]);
      if ($validator->fails()) {
        $fail = "";
        foreach ($validator->errors()->all() as $key => $error) {
            $key++;
            $fail .= "".$error."\n";
        }
        return response()->json([
            'validate' => false,
            'message' => $fail
        ]);
      }
      else {
        $user = JWTAuth::toUser(JWTAuth::getToken());
        
        $solicitud = [
          'descripcion_solicitud'=>$request->descripcion_solicitud,
          'fecha'=>Carbon::now(),
          'aprobado'=>0,
          'id_servicio'=>$request->colaborador['id_servicio'],
          'id_colaborador'=>$request->colaborador['id'],
          'id_usuario'=>$user->id,
        ];

        $solicitude_creada = Solicitude::create($solicitud);
        return response()->json([
            'validate' => true,
            'message' => 'Solicitud creada con exito'
        ]);
      }
    }

    public function solicitudes_pendientes(){
      $solicitudes_pendientes = Solicitude::where('aprobado',0)->get();
      if (count($solicitudes_pendientes) <= 0) {
        return response()->json([
            'validate' => false,
            'message' => 'No hay solicitudes pendientes'
        ]);
      }
      else{
        for ($i=0; $i < count($solicitudes_pendientes); $i++) { 
          $user = User::find($solicitudes_pendientes[$i]['id_usuario']);
          $solicitudes_pendientes[$i]['id_usuario'] = $user;
        }
        return response()->json([
            'validate' => true,
            'data' => $solicitudes_pendientes,
        ]);
      }
    }

    public function solicitudes_aprobadas(){
     $solicitudes_pendientes = Solicitude::where('aprobado',1)->get();
     if (count($solicitudes_pendientes) <= 0) {
       return response()->json([
            'validate' => false,
            'message' => 'No hay solicitudes aprobadas'
        ]);
     }
     else{
      for ($i=0; $i < count($solicitudes_pendientes); $i++) { 
        $user = User::find($solicitudes_pendientes[$i]['id_usuario']);
        $solicitudes_pendientes[$i]['id_usuario'] = $user;
      }
      return response()->json([
          'validate' => true,
          'data' => $solicitudes_pendientes,
      ]); 
     }
    }

    public function solicitudes(){
     $solicitudes_pendientes = Solicitude::all();
     if (count($solicitudes_pendientes) <= 0) {
       return response()->json([
            'validate' => false,
            'message' => 'No hay solicitudes contratadas'
        ]);
     }
     else{
      for ($i=0; $i < count($solicitudes_pendientes); $i++) {
        $colaborador = Colaborador::find($solicitudes_pendientes[$i]['id_colaborador']);
        $user = User::find($colaborador['id_usuario']);
        $solicitudes_pendientes[$i]['id_usuario'] = $user;
      }
      return response()->json([
          'validate' => true,
          'data' => $solicitudes_pendientes,
      ]); 
     } 
    }

    public function aceptar_solicitud(Request $request){
      $solicitud = Solicitude::find($request->id);
      $solicitud->aprobado = $request->aprobado;
      $solicitud->update();
      return response()->json([
            'validate' => true,
            'message' => 'Solicitud aprobada'
        ]);
    }

    public function cancelar_solicitud(Request $request){
      $solicitud = Solicitude::find($request->id);
      $solicitud->aprobado = $request->aprobado;
      $solicitud->update();
      return response()->json([
            'validate' => true,
            'message' => 'Solicitud rechazada'
        ]);
    }
}
