<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Validator;
use JWTFactory;
use JWTAuth;
use App\User;
use App\Rol;
use App\Ubicacion;
use Carbon\Carbon;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        if ($request->operacion == 'login') {
            $validator = Validator::make($request->all(), [
                'email' => 'required|string|email|max:255',
                'password'=> 'required|string|min:6'
            ]);
            if ($validator->fails()) {
              $fail = "";
              foreach ($validator->errors()->all() as $key => $error) {
                  $key++;
                  $fail .= "".$error."\n";
              }

              return response()->json([
                  'validate' => false,
                  'message' => $fail
              ]);
                //return response()->json(['validate' => false,'message' => $validator->errors()]);
            }

            $credentials = $request->only('email', 'password');
            try {
              if ($token = $this->guard()->attempt($credentials)) {
                //$user = JWTAuth::authenticate($token);
                $user = Auth::user();
                $rol = Rol::select('descripcion')->find($user->id_rol)->descripcion;
                return response()->json([
                  'validate' => true,
                  'username' => $user->nombre.' '.$user->apellido,
                  'role' => $rol,
                  'token' => $token
                ]);
                  //return $this->respondWithToken($token);
              }

              return response()->json([
                        'validate' => false,
                        'message' => 'Correo o contraseña incorrecta.'], 401);
              //return response()->json(['error' => 'Usuario o contrasenia incorrecta'], 401);

            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }
        }
        if ($request->operacion == 'registrarse') {
            $foto_perfil = '';
            $validator = Validator::make($request->all(), [
                'nombre' => 'required|string',
                'apellido' => 'required|string',
                'email' => 'required|email|unique:users',
                'telefono' => 'required',
                'sexo' => 'required',
                'password' => 'required',
                'password2' => 'required',
                'id_rol' => 'required',
                'id_departamento' => 'required',
                'colonia' => 'required',
                'direccion_especifica' => 'required',
                'latitud' => 'required',
                'longitud' => 'required',
            ]);
            if ($validator->fails()) {
              $fail = "";
              foreach ($validator->errors()->all() as $key => $error) {
                  $key++;
                  $fail .= "".$error."\n";
              }

              return response()->json([
                  'validate' => false,
                  'message' => $fail
              ]);
            }
            else{
              $ubicacion = ['latitud'=>$request->latitud,
                            'longitud'=>$request->longitud,
                            'id_departamento'=>$request->id_departamento,
                            'colonia'=>$request->colonia,
                            'direccion_especifica'=>$request->direccion_especifica];

              $ubicacion_creada = Ubicacion::create($ubicacion);

              $foto_perfil = 'null';

              if ($request->password != $request->password2) {
                return response()->json([
                    'validate' => false,
                    'message' => 'Las contraseñas no coinciden']);
              }

              $user = ['nombre'=>$request->nombre,
                        'apellido'=>$request->apellido,
                        'email'=>$request->email,
                        'telefono'=>$request->telefono,
                        'sexo'=>$request->sexo,
                        'foto_perfil'=>$foto_perfil,
                        'password'=>bcrypt($request->password),
                        'fecha_inscripcion'=>Carbon::now(),
                        'activo'=>1,
                        'id_rol'=>$request->id_rol,
                        'id_ubicacion'=>$ubicacion_creada->id,
                      ];

              $user_creado = User::create($user);

              return response()->json([
                  'validate' => true,
                  'message' => 'Usuario insertado con exito!'
              ]);
        }
    }
  }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json($this->guard()->user());
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }

}
