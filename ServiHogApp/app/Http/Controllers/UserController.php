<?php

namespace App\Http\Controllers;
use JWTFactory;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use App\User;
use App\Rol;
use Illuminate\Http\Request;

class UserController extends Controller
{

  //funcion para obtener todos los usuarios en la base de datos (filtrado por roles).
  public function obtener_usuarios(Request $request)
  {
    $validator = Validator::make($request->all(), [
        'id_rol' => 'required'
    ]);
    if ($validator->fails()) {
        return response()->json([
            'error' => 'true',
            'mensaje' => $validator->errors()]);
    }
    else{
      $users = User::orderBy('id', 'ASC')->where('id_rol',$request->get('id_rol'))->get();
      return response()->json([
          'error' => 'false',
          'usuarios' => $users
      ]);
    }
  }

  /**
     * Mediante el rol obtenido del token, se prepara un arreglo
     * con los diferentes accesos de cada usuario.
     * Cada objeto del arreglo será interpretado por angular 1
     * por eso tiene ese formato
     */
    public function getMenu()
    {
        //$user = JWTAuth::parseToken()->authenticate();
        $user = JWTAuth::toUser(JWTAuth::getToken());
        //$user = Auth::user();
        //$user = JWTAuth::authenticate();
        $rol = Rol::select('descripcion')->find($user->id_rol)->descripcion;
        $menu = array();
        switch($rol){
            case "Cliente":
                array_push($menu,
                    ['titulo'=> ' Servicios', 'icono' => 'fas fa-hand-holding fa-fw', 'mostrar' => false,
                    'hijos' => array(['estado' => 'main.cliente', 'titulo' => 'Buscar Servicios'],
                     ['estado' => 'main.servicios_solicitados', 'titulo' => 'Servicios adquiridos'])],
                     ['titulo'=> ' Perfil', 'icono' => 'fas fa-user-tie fa-fw', 'mostrar' => false,
                    'hijos' => array(['estado' => 'main.perfil_cliente', 'titulo' => 'Ver Perfil'],
                     ['estado' => 'main.perfil_modificar', 'titulo' => 'Modificar Perfil'])]);
            break;

            case "Colaborador":
                array_push($menu,
                    ['titulo'=> ' Solicitudes', 'icono' => 'fas fa-briefcase fa-fw',
                     'mostrar' => false,
                    'hijos' => array(['estado' => 'main.colaborador', 'titulo' => 'Clientes pendientes'],
                     ['estado' => 'main.historialcolaborador', 'titulo' => 'Solicitudes aceptadas'])],

                    ['titulo'=> ' Configuracion', 'icono' => 'fas fa-cogs fa-fw', 'mostrar' => false,
                    'hijos' => array(['estado' => 'main.cambiar_servicio', 'titulo' => 'Servicio Ofrecido'],
                     ['estado' => 'main.perfil', 'titulo' => 'Perfil'])]
                    );
                break;

            case "Administrador":
              array_push($menu,
                    ['titulo'=> ' Solicitudes', 'icono' => 'fas fa-briefcase fa-fw',
                     'mostrar' => false,
                    'hijos' => array(['estado' => 'main.admin_solicitudes', 'titulo' => 'Solicitudes Pendientes'],
                     ['estado' => 'main.historialcolaborador', 'titulo' => 'Contratos realizados'])],

                    ['titulo'=> ' Usuarios', 'icono' => 'fas fa-users fa-fw', 'mostrar' => false,
                    'hijos' => array(['estado' => 'main.ver_usuarios', 'titulo' => 'Ver Usuarios'])],

                    ['titulo'=> ' Servicios', 'icono' => 'far fa-handshake fa-fw', 'mostrar' => false,
                    'hijos' => array(['estado' => 'main.ver_servicios_admin', 'titulo' => 'Ver Servicios'],
                    ['estado' => 'main.modificar_servicios_admin', 'titulo' => 'Agregar Servicio'])]);
            break;
        }

        return response()->json([
            'validate' => true,
            'menu' => $menu,
            'username' =>$user->nombre.' '.$user->apellido,
            'user' =>$user,
            'rol' =>$rol
        ]);
    }


}
