<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Contrato extends Model
{
    use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'id','puntuacion','fecha','duracion','descripcion','estado','cancelado','id_ubicacion','id_cobro','id_colaborador','id_usuario','id_solicitud'];
}
