<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Colaborador extends Model
{
    
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'id', 'puntuacion','hoja_de_vida','descripcion','habilitado','id_usuario','id_ubicacion','id_escolaridad','id_servicio',
  ];
}
