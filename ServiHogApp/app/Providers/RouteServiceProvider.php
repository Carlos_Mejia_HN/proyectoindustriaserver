<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
      // Route::prefix('api/auth/')->group(function () {
      // });
        // Route::prefix('api')
        //      ->middleware('api')
        //      ->namespace($this->namespace)
        //      ->group(base_path('routes/api.php'));
        Route::group([
               'middleware' => ['api'],
               'namespace' => $this->namespace,
               'prefix' => 'api/auth/',
        ], function ($router) {
                //Add you routes here, for example:
                //Route::apiResource('/posts','PostController');
                Route::post('login', 'AuthController@login');
                Route::post('logout', 'AuthController@logout');
                Route::post('refresh', 'AuthController@refresh');
                Route::post('obtener_usuarios', 'UserController@obtener_usuarios');
                Route::get('menu/rol', 'UserController@getMenu');
                Route::get('categorias', 'ClienteController@getCategorias');
                Route::post('obtener_servicios','ClienteController@getServicios');
                Route::post('obtener_colaboradores','ClienteController@obtener_colaboradores');
                Route::get('get_usuarios','AdministradorController@get_usuarios');
                Route::post('actualizar_usuario','AdministradorController@actualizar_usuario');
                Route::get('get_servicios','AdministradorController@get_servicios');
                Route::post('contratar_colaborador','ClienteController@contratar_colaborador');
                Route::get('solicitudes_pendientes','ClienteController@solicitudes_pendientes');
                Route::get('solicitudes_aprobadas','ClienteController@solicitudes_aprobadas');
                Route::get('solicitudes','ClienteController@solicitudes');
                Route::post('aceptar_solicitud','ClienteController@aceptar_solicitud');
                Route::post('cancelar_solicitud','ClienteController@cancelar_solicitud');
        });
    }
}
