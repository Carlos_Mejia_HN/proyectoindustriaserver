<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Solicitude extends Model
{
    use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['id','descripcion_solicitud','fecha','aprobado','id_servicio','id_colaborador','id_usuario'];
}
