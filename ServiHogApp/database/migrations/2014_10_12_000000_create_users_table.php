<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellido');
            $table->string('email');
            $table->string('telefono');
            $table->string('sexo');
            $table->string('foto_perfil');
            $table->string('password');
            $table->date('fecha_inscripcion');
            $table->boolean('activo');
            $table->unsignedInteger('id_rol');
            $table->foreign('id_rol')->references('id')->on('rols');
            $table->unsignedInteger('id_ubicacion');
            $table->foreign('id_ubicacion')->references('id')->on('ubicacions')->onDelete('cascade');
            //$table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
