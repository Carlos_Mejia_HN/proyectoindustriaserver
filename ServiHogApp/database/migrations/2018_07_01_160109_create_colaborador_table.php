<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColaboradorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colaboradors', function (Blueprint $table) {
            $table->increments('id');
            $table->float('puntuacion');
            $table->string('hoja_de_vida');
            $table->string('descripcion');
            $table->boolean('habilitado');
            $table->unsignedInteger('id_usuario');
            $table->foreign('id_usuario')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('id_ubicacion');
            $table->foreign('id_ubicacion')->references('id')->on('ubicacions')->onDelete('cascade');
            $table->unsignedInteger('id_escolaridad');
            $table->foreign('id_escolaridad')->references('id')->on('escolaridades')->onDelete('cascade');
            $table->unsignedInteger('id_servicio');
            $table->foreign('id_servicio')->references('id')->on('servicios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colaboradors');
    }
}
