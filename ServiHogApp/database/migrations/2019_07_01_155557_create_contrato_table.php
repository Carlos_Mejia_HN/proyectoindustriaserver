<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {
            $table->increments('id');
            $table->float('puntuacion');
            $table->date('fecha');
            $table->time('duracion');
            $table->string('descripcion');
            $table->string('estado');
            $table->boolean('cancelado');
            $table->unsignedInteger('id_ubicacion');
            $table->foreign('id_ubicacion')->references('id')->on('ubicacions')->onDelete('cascade');
            $table->unsignedInteger('id_cobro');
            $table->foreign('id_cobro')->references('id')->on('cobros')->onDelete('cascade');
            $table->unsignedInteger('id_colaborador');
            $table->foreign('id_colaborador')->references('id')->on('colaboradors');
            $table->unsignedInteger('id_usuario');
            $table->foreign('id_usuario')->references('id')->on('users');
            $table->unsignedInteger('id_solicitud');
            $table->foreign('id_solicitud')->references('id')->on('solicitudes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratos');
    }
}
