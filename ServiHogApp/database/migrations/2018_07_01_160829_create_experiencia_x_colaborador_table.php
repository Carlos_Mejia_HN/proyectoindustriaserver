<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperienciaXColaboradorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiencia_x_colaborador', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_colaborador');
            $table->foreign('id_colaborador')->references('id')->on('colaboradors');
            $table->unsignedInteger('id_experiencia');
            $table->foreign('id_experiencia')->references('id')->on('experiencias');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experiencia_x_colaborador');
    }
}
