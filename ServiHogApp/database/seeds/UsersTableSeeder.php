<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
          'nombre' => 'Carlos',
          'apellido' => 'Mejia',
          'email' => 'asd1@gmail.com',
          'telefono' => '32454316',
          'sexo' => 'M',
          'foto_perfil' => 'null',
          'password' => bcrypt('secret'),
          'fecha_inscripcion' => Carbon::parse('2018-08-17'),
          'activo' => 1,
          'id_rol' => 1,
          'id_ubicacion' => 1,
      ]);

      DB::table('users')->insert([
          'nombre' => 'Marian',
          'apellido' => 'Padilla',
          'email' => 'asd2@gmail.com',
          'telefono' => '32454316',
          'sexo' => 'F',
          'foto_perfil' => 'null',
          'password' => bcrypt('secret'),
          'fecha_inscripcion' => Carbon::parse('2018-08-17'),
          'activo' => 1,
          'id_rol' => 2,
          'id_ubicacion' => 1,
      ]);
    }
}
