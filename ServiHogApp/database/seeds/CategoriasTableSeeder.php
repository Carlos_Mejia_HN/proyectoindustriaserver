<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categoria_servicios')->insert([
        'id' => 1,
        'descripcion' => 'Tecnologia',
        'imagen' =>'null',
      ]);

      DB::table('categoria_servicios')->insert([
        'id' => 2,
        'descripcion' => 'Fontaneria',
        'imagen' =>'null',
      ]);

      DB::table('categoria_servicios')->insert([
        'id' => 3,
        'descripcion' => 'Limpieza',
        'imagen' =>'null',
      ]);

      DB::table('categoria_servicios')->insert([
        'id' => 4,
        'descripcion' => 'Construccion / Reparacion',
        'imagen' =>'null',
      ]);

      DB::table('categoria_servicios')->insert([
        'id' => 5,
        'descripcion' => 'Atencion y Cuidado',
        'imagen' =>'null',
      ]);

      DB::table('categoria_servicios')->insert([
        'id' => 6,
        'descripcion' => 'Electricidad / Electronica',
        'imagen' =>'null',
      ]);

      DB::table('categoria_servicios')->insert([
        'id' => 7,
        'descripcion' => 'Pintura',
        'imagen' =>'null',
      ]);

      DB::table('categoria_servicios')->insert([
        'id' => 8,
        'descripcion' => 'Educacion',
        'imagen' =>'null',
      ]);

      DB::table('categoria_servicios')->insert([
        'id' => 9,
        'descripcion' => 'Salud y Belleza',
        'imagen' =>'null',
      ]);
    }
}
