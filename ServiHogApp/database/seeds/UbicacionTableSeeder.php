<?php

use Illuminate\Database\Seeder;

class UbicacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('ubicacions')->insert([
        'id' => 1,
        'latitud' => 	14.16226,
        'longitud' => -87.04085,
        'id_departamento' => 1,
        'colonia' => 'Valle de Angeles',
        'direccion_especifica' => 'Bo. el zarzal bloque 1 casa 3',
      ]);
    }
}
