<?php

use Illuminate\Database\Seeder;

class RolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rols')->insert([
          'id' => 1,
          'descripcion' => 'Cliente',
        ]);
        DB::table('rols')->insert([
          'id' => 2,
          'descripcion' => 'Colaborador',
        ]);
        DB::table('rols')->insert([
          'id' => 3,
          'descripcion' => 'Administrador',
        ]);
    }
}
