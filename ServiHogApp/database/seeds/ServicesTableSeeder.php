<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('servicios')->insert([
        'id' => 1,
        'nombre' => 	'Tutorias',
        'descripcion' => 'Tutorias de matematica',
        'id_categoria' => 8,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

      DB::table('servicios')->insert([
        'id' => 2,
        'nombre' =>     'Musica',
        'descripcion' => 'Clases de Guitarra',
        'id_categoria' => 8,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

      DB::table('servicios')->insert([
        'id' => 3,
        'nombre' =>     'Reparacion de PC/impresoras',
        'descripcion' => 'Reparacion de PC/impresoras',
        'id_categoria' => 1,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

      DB::table('servicios')->insert([
        'id' => 4,
        'nombre' =>     'Instalacion de Software',
        'descripcion' => 'Instalacion de sistema operativo, backup, analisis contra virus',
        'id_categoria' => 1,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

      DB::table('servicios')->insert([
        'id' => 6,
        'nombre' =>     'Reparación tuberias baño/lavados',
        'descripcion' => 'Reparación tuberias baño/lavados',
        'id_categoria' => 2,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

      DB::table('servicios')->insert([
        'id' => 7,
        'nombre' =>     'Instalacion de tuberias',
        'descripcion' => 'Instalacion de tuberias',
        'id_categoria' => 2,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

      DB::table('servicios')->insert([
        'id' => 5,
        'nombre' =>     'Aseo completo',
        'descripcion' => 'Aseo completo, limpieza del hogar, de bodegas, de oficinas.',
        'id_categoria' => 3,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

      DB::table('servicios')->insert([
        'id' => 8,
        'nombre' =>     'Lavado',
        'descripcion' => 'Lavado de ropa a mano o lavadora, lavado de carro',
        'id_categoria' => 3,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

      DB::table('servicios')->insert([
        'id' => 9,
        'nombre' =>     'Reparacion/Instalacion de Techos',
        'descripcion' => 'Reparacion/Instalacion de Techos',
        'id_categoria' => 4,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

      DB::table('servicios')->insert([
        'id' => 10,
        'nombre' =>     'Instalacion de Ceramica',
        'descripcion' => 'Instalacion de Ceramica',
        'id_categoria' => 4,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

      DB::table('servicios')->insert([
        'id' => 11,
        'nombre' =>     'Cuidado de niños',
        'descripcion' => 'Cuidado de niños por dias, por horas, por semanas',
        'id_categoria' => 5,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

      DB::table('servicios')->insert([
        'id' => 12,
        'nombre' =>     'Cuidado de ancianos',
        'descripcion' => 'Con especialidad en enferemeria para la aplicacion de medicamentos',
        'id_categoria' => 5,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

      DB::table('servicios')->insert([
        'id' => 13,
        'nombre' =>     'Instalacion electrica',
        'descripcion' => 'Instalacion electrica dentro del hogar, adaptando a trifasicos',
        'id_categoria' => 6,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

      DB::table('servicios')->insert([
        'id' => 14,
        'nombre' =>     'Reparacion electrodomesticos',
        'descripcion' => 'Revision y reparacion de estufas, refrigeradoras, televisores, microondas',
        'id_categoria' => 6,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

      DB::table('servicios')->insert([
        'id' => 15,
        'nombre' =>     'Pintura Interiores/exteriores',
        'descripcion' => 'Pintar la casa completa o secciones, de agua o aceite',
        'id_categoria' => 7,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

      DB::table('servicios')->insert([
        'id' => 16,
        'nombre' =>     'Pintura y barnizado madera',
        'descripcion' => 'Manejo de cualquier tipo de madera',
        'id_categoria' => 7,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

      DB::table('servicios')->insert([
        'id' => 17,
        'nombre' =>     'Spa-masajes, manicura, pedicura',
        'descripcion' => 'Llevamos la relajacion hasta tu casa, masajes terapeutas y cuidados de la piel',
        'id_categoria' => 9,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

      DB::table('servicios')->insert([
        'id' => 18,
        'nombre' =>     'Peluqueria',
        'descripcion' => 'Corte de pelo a niños, mujeres y hombres, secados, alisados, teñido',
        'id_categoria' => 9,
        'servicio_domicilio' => 1,
        'imagen' => 'null',
      ]);

    }
}
