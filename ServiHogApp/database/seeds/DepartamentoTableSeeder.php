<?php

use Illuminate\Database\Seeder;

class DepartamentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('departamentos')->insert([
        'id' => 1,
        'departamento' => 'Francisco Morazan',
      ]);
    }
}
